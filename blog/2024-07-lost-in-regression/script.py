"""
Figures for the regression blogpost.
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
import pymc as pm
import arviz as az

rng = np.random.default_rng(seed=42)

mean = [0, 0]
cov = np.array([[1, 0.5], [0.5, 1]])
xs, ys = rng.multivariate_normal(mean=mean, cov=cov, size=1000).T

betax = cov[0, 1] / cov[0, 0]
betay = cov[0, 1] / cov[1, 1]

# with pm.Model("model_yx") as model_yx:
#     beta_x = pm.Normal("beta_x")
#     sigma_y = pm.HalfNormal("sigma_y")
#     pm.Normal("y", beta_x * xs, sigma_y, observed=ys)
#     idata_yx = pm.sample()

# print(az.summary(idata_yx))

# with pm.Model("model") as model:
#     sigma_x = pm.HalfNormal("sigma_x")
#     sigma_y = pm.HalfNormal("sigma_y")
#     beta = pm.Normal("beta")
#     x = pm.Normal("x", xs, sigma_x)
#     pm.Normal("y", beta * x, sigma_y, observed=ys)
#     idata = pm.sample()

# print(az.summary(idata, var_names=["model::sigma_x", "model::sigma_y", "model::beta"]))

# assert False

mmx = np.array([min(xs), max(xs)])
mmy = np.array([min(ys), max(ys)])
mm = np.array([max(xs.min(), ys.min()), min(xs.max(), ys.max())])

fig, axs = plt.subplot_mosaic(
    [["anim"] * 3, ["start", "res1", "res2"]],
    figsize=(8, 7),
    height_ratios=[3, 1],
    layout="constrained",
)

scat = axs["anim"].scatter(xs, ys, alpha=0.5)
[y_x] = axs["anim"].plot(mmx, betax * mmx, color="C1", lw=4)
[x_y] = axs["anim"].plot(betay * mmy, mmy, color="C2", lw=4)
[middle] = axs["anim"].plot(mm, mm, color="C4", lw=4)
axs["anim"].set_xlabel(r"$x$")
axs["anim"].set_ylabel(r"$y - \alpha\,x$")
axs["anim"].grid()

axs["start"].scatter(xs, ys, alpha=0.25, s=8)
axs["start"].plot(mmx, betax * mmx, color="C1", lw=3)
axs["start"].plot(betay * mmy, mmy, color="C2", lw=3)
axs["start"].plot(mm, mm, color="C4", lw=3)
axs["start"].set_xlabel(r"$x$")
axs["start"].set_ylabel(r"$y$")
axs["start"].grid()

axs["res1"].scatter(xs, ys - betax * xs, alpha=0.25, s=8)
axs["res1"].plot(mmx, betax * mmx - betax * mmx, color="C1", lw=3)
axs["res1"].plot(betay * mmy, mmy - betax * betay * mmy, color="C2", lw=3)
axs["res1"].plot(mm, mm - betax * mm, color="C4", lw=3)
axs["res1"].set_xlabel(r"$x$")
axs["res1"].set_ylabel(r"$y-\beta_x\,x$")
axs["res1"].grid()

axs["res2"].scatter(xs, ys - xs, alpha=0.25, s=8)
axs["res2"].plot(mmx, betax * mmx - mmx, color="C1", lw=3)
axs["res2"].plot(betay * mmy, mmy - betay * mmy, color="C2", lw=3)
axs["res2"].plot(mm, mm - mm, color="C4", lw=3)
axs["res2"].set_xlabel(r"$x$")
axs["res2"].set_ylabel(r"$y-x$")
axs["res2"].grid()

pause = 10

frames = [
    *([0] * pause),
    *range(25),
    *([25] * pause),
    *range(25, 50),
    *([50] * (pause + 1)),
    *reversed(range(25, 50)),
    *([25] * pause),
    *reversed(range(1, 25)),
    0,
]


def update(frame: int):
    speed = 1 / 50
    data = np.stack([xs, ys - speed * frame * xs]).T
    scat.set_offsets(data)
    y_x.set_ydata(betax * mmx - speed * frame * mmx)
    x_y.set_ydata(mmy - speed * frame * betay * mmy)
    middle.set_ydata(mm - speed * frame * mm)
    return [scat, y_x, x_y, middle]


ani = animation.FuncAnimation(fig=fig, func=update, frames=frames, interval=100 / 3)

ani.save("video.mp4")
