---
draft: true
title: Water sort
date: 2021-02-07
description: "Solving a puzzle with path search algorithms."
categories: [puzzle, optimization]
---

I recently installed a game on my phone.  It is a succession of puzzles
to solve, where coloured fluids in test tubes have to be unmixed by
pouring them layer by layer, from one tube to the other, while obeying a
set of simple rules.

It started easy, but some problems are harder and the wrong set of moves
sometimes leads to a position where no more progress can be made.  I
then wondered how difficult it was to come up with a starting position
that can be solved.  Surely, all the puzzles in the application have a
solution, but how difficult was it to find such puzzles?

A method to be sure to obtain solvable puzzles is to start from a solved
configuration, and apply a given number of "reverse moves", i.e. moves such
that the opposite move is a legal game move.  With this method, there will
always be at least one solution.

However, I have sometimes been stuck while trying to solve problems,
so I know that some positions do not admit solutions.  I wanted
to determine the relative proportion of solvable and not solvable
positions.  In other words, given a random initial mix of colours,
is it more probable that the puzzle can be solved or not?

## Rules of the game

The game starts with a given number of test tubes fully filled with 4
layers of fluids of various colours, plus two empty tubes.  One can
pour the upper layers of a tube into another, if and only if they are
the same colour as the upper layer of the destination tube, or if the
destination tube is empty.

The goal of the game is to end up in a situation where all tubes are
either full of the same colour, or empty.

![Screenshot of the initial state of the fourth puzzle](water-sort.jpg){width=40%}

## Solution search

There might be several ways to determine the proportion of solvable
puzzles among random configurations, but the easiest and most intuitive
is probably direct sampling.  We will randomly shuffle the colours
numerous times, and for each initial configuration, determine whether or
not it is solvable.  For this, we need an algorithm to solve a puzzle.

The state of the game is entirely described by the state of the tubes.
From each state, one can make a number of possible moves each resulting
in an other game state.  We are searching for a series of moves
that results in a game state with particular constraints (a winning
configuration with unmixed water).

For a given puzzle, we can think of each reachable game configuration
as a node of a graph, with a directed edge between two nodes when the
second is reachable from the first in one move.  Solving the problem
corresponds to searching for a path in this graph from the initial node
to a node with the characteristics of a final state.

### Breadth-first search

The first algorithm that we will try is Breadth-first search (BFS).  Its
concept is simple: starting from the initial position, determine all
positions reachable in one move.  From these new positions, determine
all new positions reachable in one move from them (they are then all
reachable in two moves (or possibly more, but not less) from the initial
position).  Iterate this search to compute all the positions reachable
in 3, 4, 5 moves, etc.  The first time that we see a final state, we
know that we could not have got there earlier, hence it is (one of) the
shortest solutions.

I visualize this algorithm as expanding a bubble in the graph around the
initial state.  We search in the dark: nothing ever tells us if we are
getting close to a solution, we will only know when we are there.  So
the best we can do is explore uniformly and fairly around the start, one
step at a time.

With this algorithm, we find that the shortest solution to level 4
has 10 moves.  Before reaching it, we visited 1432 different game
states.  This number quickly increases as the number of initially
filled tubes increases, reaching an average of almost one million for
7-colour puzzles.  This corresponds to a solving time of several tens of
seconds, which is too long if we want to solve hundreds or thousands of
problems.  So we need to go faster.

### A* algorithm

Fortunately, a way to go faster is to replace BFS with the A* ("A star")
algorithm.  It is a cousin of BFS, but whereas BFS's search is uninformed
(it expands the search bubble uniformly), A*'s search is informed by a
heuristic, which suggests it in which direction to go.

In practice, the heuristic is a user-defined function which should
estimate the length of the shortest path to the solution, from any given
state.  A* takes into account this information to guide the search.
Concretely, it explores the states in increasing order of distance from
the initial point **plus** estimated distance to a solution.

We can see here that if the heuristic is the constant zero, A* is
actually BFS.  On the opposite, if the heuristic is exact and computes
exactly the distance to the solution, then A* will go to the solution in
a straight path.  However, it is often as difficult to compute an exact
heuristic as to solve the problem, so the heuristic is in general an
approximation.

It can be proven that A* converges to the optimal solution only if the
heuristic function never overestimates the distance to the solution
(such heuristics are called "admissible").  If there is a state such
that the heuristic overestimates the distance from this state to the
solution, then another state might be explored before this one, which
can lead to a suboptimal solution.

So the goal when using A* is to determine how to compute quickly the
best possible under-approximation of the number of moves that it will
take to solve a puzzle.  I suggest you to take a moment now to think
about a heuristic for this problem!

#### First heuristic

The first heuristic that came to my mind is to consider the number of
colours that are not yet in a single tube.  For sure, one needs one move
or more to solve each of the colours that are not yet solved, which
means that this heuristic is admissible.

Running this algorithm, we also find a solution in 10 moves (obviously),
but more efficiently because we visited this time only 486 states.

Here is a view of the solution, H1 showing the value of this first
heuristic for each state:

```
                Tube 1    Tube 2    Tube 3    Tube 4    Tube 5  H1
Initial state    BROO      BRBR      OBRO      ....      ....    3
Step  1: 1 → 4   BR..      BRBR      OBRO      OO..      ....    3
Step  2: 1 → 5   B...      BRBR      OBRO      OO..      R...    3
Step  3: 3 → 4   B...      BRBR      OBR.      OOO.      R...    3
Step  4: 3 → 5   B...      BRBR      OB..      OOO.      RR..    3
Step  5: 3 → 1   BB..      BRBR      O...      OOO.      RR..    3
Step  6: 3 → 4   BB..      BRBR      ....      OOOO      RR..    2
Step  7: 2 → 5   BB..      BRB.      ....      OOOO      RRR.    2
Step  8: 2 → 1   BBB.      BR..      ....      OOOO      RRR.    2
Step  9: 2 → 5   BBB.      B...      ....      OOOO      RRRR    1
Step 10: 1 → 2   ....      BBBB      ....      OOOO      RRRR    0
```

Here, one can see that the heuristic, although admissible, could be
subject to improvement.  Indeed, its value did not change until the 6th
step.  This doesn't seem right: surely we were closer to the solution on
step 5 than on the initial state!  How to quantify that?

#### A better heuristic

With a bit more thought, it is indeed possible to create a better
heuristic.  This one counts the number of "colour blobs": here,
initially, there is 3 in the first tube (B, R, and OO), 4 in the second
(B, R, B, and R) and 4 in the third (O, B, R, and O), for a total of 11.
The puzzle is solved when the number of colour blobs is equal to the
number of colours, here 3.  Furthermore, each move can only reduce this
number by at most 1.  So the total number of colour blobs, **minus** the
total number of colours, is an admissible heuristic.

With my implementation, A* returns with this heuristic a solution of the
same length but after only 36 visits of states!  For comparison, here is
the solution, with H2 the new heuristic:

```
                Tube 1    Tube 2    Tube 3    Tube 4    Tube 5  H1  H2
Initial state    BROO      BRBR      OBRO      ....      ....    3   8
Step  1: 1 → 4   BR..      BRBR      OBRO      OO..      ....    3   8
Step  2: 2 → 1   BRR.      BRB.      OBRO      OO..      ....    3   7
Step  3: 3 → 4   BRR.      BRB.      OBR.      OOO.      ....    3   6
Step  4: 3 → 1   BRRR      BRB.      OB..      OOO.      ....    3   5
Step  5: 3 → 2   BRRR      BRBB      O...      OOO.      ....    3   4
Step  6: 4 → 3   BRRR      BRBB      OOOO      ....      ....    2   3
Step  7: 1 → 5   B...      BRBB      OOOO      ....      RRR.    2   3
Step  8: 2 → 1   BBB.      BR..      OOOO      ....      RRR.    2   2
Step  9: 2 → 5   BBB.      B...      OOOO      ....      RRRR    1   1
Step 10: 1 → 2   ....      BBBB      OOOO      ....      RRRR    0   0
```

One can see here that the second heuristic clearly guides the search by
decreasing at almost every step.  This is a near ideal situation.

I ran these algorithms several times for different initial situations,
with a number of colours varying between 2 and 9, and plotted the number
of visited states during the solving of the problem.  The difference
between BFS and A* with the first and the second heuristic is rather
obvious, as seen on the following figure:

![Comparison of BFS and A* with two different heuristics (blue = BFS, orange = A*/H1, green = A*/H2)](comparison_algos.svg)

## So, these impossible positions?

I didn't generate any problem that could not be solved, with 100 random
problems from 2 to 9 colours.  This does not mean that they do not
exist, just that they are rare for these numbers.

### Smallest impossible position

#### Is it even reachable?

### Smallest impossible initial state


## Why is green bimodal??


