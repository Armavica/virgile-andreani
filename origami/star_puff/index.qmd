---
title: Star puff
description: A tessellation with puffy stars.
author: Ralf Konrad
date: 2023-07-04
categories: [tessellation]
---

One tessellation where the verso looks really surprising.

It is possible to puff any of the hexagons, just not two adjacent ones.

One way to make this tessellation is to first start with a grid of everlapping
twisted hexagons (2 crowns on the rector but only one on the verso), and then
to transform every corner with triangle twists.  I found that the easiest way to
make the initial hexagonal tessellation is to make them row after row: in this
way, during folding, the unfolded part of the paper alternates between two sets
of parallel creases.

![A transparency shot of the origami tessellation in front of a desk lamp.](star_puff_transparency.jpg)

![Recto of the tessellation.](star_puff_recto.jpg)

![Verso of the tessellation.](star_puff_verso.jpg)
