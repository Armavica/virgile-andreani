---
title: 5-mill
description: A pretty double-twist starting from a pentagon.
author: Virgile Andreani
date: 2023-06-23
categories: [twist, pentagon, crease pattern]
---

![A transparency shot of the origami star in front of a desk lamp.](5mill.jpg)

![Simplified crease pattern.](5mill_cp.svg)
