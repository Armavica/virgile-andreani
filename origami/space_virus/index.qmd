---
title: Space Virus by Xander Perrott
author: Xander Perrott
date: 2021-10-25
categories: [modular]
---

![](space_virus.jpg)

Taught by Xander Perrott at the World Origami Days 2021.

Diagramme also available in the author's book "Folded Forms".
